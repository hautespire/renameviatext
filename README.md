# RenameViaText

Mass- or individually-edit filenames in a directory by editing lines in a text editor. The focus is on simplicity and speed.
The editor supports quick actions like find-and-replace and multi-line editing.

Future:
*Helpful error handling and notifications
*Support subdirectories